# encoding: utf-8
require 'pluto/models'
require 'nokogiri'

puts "db settings:"
@db_config = {
  adapter: 'sqlite3',
  database: './planet.db'
}

pp @db_config


def run( args )
  unless File.exists?( @db_config[:database])
    puts "** error: database #{@db_config[:database]} missing; please check pluto documentation for importing feeds etc."
    exit 1;
  end

  Pluto.connect( @db_config )

  Pluto::Model::Item.latest.limit(1300).each_with_index do |item,i|
    puts "[#{i+1}] #{item.title}"
    generate_post( item )
  end
end

def generate_post( item )
  lang = item.feed.location ? item.feed.location : "en"
  content_root = "./content/#{lang}"

  FileUtils.mkdir_p( content_root )  ## make sure path exists

  item.published = item.updated if item.published.nil?
  content = item.content ? item.content : item.summary
  if item.title == ''
    item.title = Nokogiri::HTML::Document.parse(content).search('//text()').first if content
    item.title = item.title.slice(0..(item.title.index('.'))) if item.title
    item.title = item.title.slice(0..255) if item.title
  end
  return unless item.title && item.published && item.url && content

  trailing = item.title.parameterize
  if trailing == ''
    trailing = Digest::SHA2.hexdigest content
  end
  fn = "#{content_root}/#{trailing}.html"

  # Check for author tags
  data = {}
  data["title"] = item.title unless item.title.empty?
  data["date"] = item.published.strftime('%FT%T%z') if item.published
  data["lastmod"] = item.updated.strftime('%FT%T%z') if item.updated
  data["guid"] = item.guid unless item.guid.empty?
  data["format"] = item.feed.format
  data["author"] = item.feed.title.empty? ? item.feed.auto_title : item.feed.title
  data["feedsmry"] = item.feed.summary if item.feed.summary && !item.feed.summary.empty?
  data["avatar"] = item.feed.avatar if item.feed.avatar
  data["siteurl"] = item.feed.url unless item.feed.url.empty?
  data["feedurl"] = item.feed.feed_url unless item.feed.feed_url.empty?
  data["posturl"] = item.url if item.url
  item.feed.author.split.each do |flair|
    if flair.include?(':')
      part = flair.split(':')
      data[part.shift] = part.join(':')
    else
      data[flair] = true
    end
  end if item.feed.author

  File.open( fn, 'w' ) do |f|
    f.write data.to_yaml
    f.write "---\n"

    # There were a few issues of incomplete html documents, nokogiri fixes that
    html = Nokogiri::HTML::DocumentFragment.parse(content).to_html
    # Make absolute links
    html.gsub!(/(?<=src=[\"\'])\/(?!\/)/, "#{/\/\/.*?(?=\/|$)/.match(item.feed.url)[0]}/")
    html.gsub!(/(?<=href=[\"\'])\/(?!\/)/, "#{/\/\/.*?(?=\/|$)/.match(item.feed.url)[0]}/")
    # Make browsers not report page as having non-HTTPS elements
    html.gsub!(/(?<=src=[\"\'])https?:/, "")
    html.gsub!(/(?<=href=[\"\'])https?:/, "")
    # Open links in new tabs
    html.gsub!(/(href=[\"\']\/\/)/, 'target="_blank" \1')
    # Make page style consistent
    html.gsub!(/<link [^>]*rel=[\"\']stylesheet[^>]+>/, "")
    html.gsub!(/<style[^>]*>.*?<\/style>/m, "")
    f.write html
  end
end

run( ARGV )
